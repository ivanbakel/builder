# Builder #
#### An automated versioning chain tool ####

Builder is a main libary for automating version declarations and semantic-versioning-compatible numbers in arbitrary languages. 

It attempts to abstract away the API layer of libraries to check for minor, major, and patch changes automatically, and produces hashchain-backed histories for version progression, similar to `git`.

## Versioning history ##

Builder tracks the versioning history - not _quite_ the version history - of a project using an object-tree system to record version entries and their corresponding numbers. Make changes to the project, and run Builder to insert a new version into the tree: the numbering is handled automatically based on how the API changed from the last build.

It's possible to travel back in the history if you make a bad build, but Builder doesn't handle changes to the file system: that's a job for your VCS. There's no equivalent to "merging", either - a new version is a one-way street.

Builder can verify that new builds are correct continuations of previous ones, and check the API changes between each. If you and another person make different changes to your projects, you end up with different versioning histories - and you can't just substitute the resulting build between them.


## Builder-ape ##

Bape (the Building Ape) is the language-specific tool used to transform a project into Builder's abstract schematic for APIs. It represents a configurable collection of tools used for each type of language and project.

### Schematic ###

TODO

## Builder-dep ##

Every good project is built on top of the work of others, whether it's the standard libraries for your language or the useful FOSS projects which support modern software. Builder-dep is a tool for tracking external dependecy use through APIs and spotting breaking changes from new builds. It works similarly to Builder-ape in that detecting dependencies is done through language-specific toolchain, but APIs can then be compared with remote manifests: if an upstream library releases a new major version, you can see if the result affects your code before even downloading it.

Builder-dep also allows for other useful behaviour - it can point out when you're relying on deprecated APIs, or when a developer has marked an API as needing looking at, even when your use of it isn't causing any build issues. Most importantly, because Builder enforces semantic versioning, Builder-dep can rely on it for telling when to update, and there's (almost) no thought involved in juggling libraries.

### Manifests ###

TODO:

### What about dependency hell? ###

Unfortunately, Builder-dep is, like all good tools, only meant to do one thing. It can't handle packages for you (not that it would try), and users are always at the mercy of their language toolchains. What builder-dep _can_ do is establish the "safe" versions for your libraries, and let you know when dependency conflict might occur. When you depend on two libraries which need different versions of yet another library, Builder-dep can find if they're compatible and the range of versions you might choose to fulfill both requirements.
