/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use semver::Version;

use core::version::version_change::{VersionChange, increment};

pub enum APIComparison {
   Change(VersionChange),
   Pre(VersionChange)
}

impl APIComparison {
    /**
     * Returns true if the version and number change are enough for this API difference
     *
     * This requires the Version started with due to the way prereleases affect ordering
     * PreSet(beta) is not an increase for x.y.z.carrot.
     */
    fn satisfied_by(&self, old_version : &Version, version_change : &VersionChange) -> bool {
        let mut new_version = old_version.clone();
        increment(&mut new_version, version_change);
        // No version change can be satisfied by a regression
        if &new_version > old_version {
            match version_change {
                // If the proposal is only a snapshot change
                // It can only satisfy snapshot changes
                change if change.is_pre() => {
                   if let &APIComparison::Pre(_) = self {
                       true
                    } else {
                        false
                    } 
                },
                // If the proposal is major/minor/patch
                // It satisfies any prerelease changes, and all changes
                // less than itself
                non_pre_change => {
                    if let &APIComparison::Change(ref change) = self {
                        change <= non_pre_change
                    } else {
                        true
                    }
                }
            }
        } else {
            false
        }
    }
}

