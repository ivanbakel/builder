/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs::File;
use std::io::Read;
use std::option::Option;
use std::result::Result;
use core::build_products::build_product::BuildProduct;

struct FileProduct {
    contents : Box<[u8]>
}

impl FileProduct {
    pub fn create(file : &File) -> Option<FileProduct> {
        let mut file_contents : Option<Vec<u8>> = Some(vec![]);

        for result in file.bytes() {
            match result {
                Result::Ok(byte) => {
                    file_contents = file_contents.map(|mut vec| { 
                        vec.push(byte);
                        vec
                    });
                }
                Result::Err(_) => {
                    file_contents = None;
                } 
            }
        }

        file_contents.map(|vec| {
            FileProduct { contents : vec.into_boxed_slice() }
        })
    }
}

impl BuildProduct for FileProduct {
    fn to_bytes(&self) -> &[u8] {
        &self.contents
    }
}

