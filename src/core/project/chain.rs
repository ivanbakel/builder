/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs;
use std::fs::File;
use std::io;
use std::io::{Error, ErrorKind, Read};
use std::path::PathBuf;
use semver::Version;

use core::version::build_version::BuildVersion;
use core::project::project::Project;

pub struct Chain<'a> {
    name : String,
    parent : &'a Project
}

impl<'a> Chain<'a> {
    pub fn create(project : &'a Project, name : &String) -> Self {
        Chain {
            parent : project,
            name : name.clone()
        }
    }

    pub fn latest_version(&self) -> io::Result<BuildVersion> {
        self.all_versions().and_then(|mut slice_box| {
            let mut versions = slice_box.into_vec();
            let max_pos = versions.iter().max_by_key(|build_version| {
                &build_version.current_version  
            }).ok_or(
                Error::new(ErrorKind::NotFound, "No versions found for this chain.")
            ).map(|version_ref| {
                // The max element is definitely in the vector
                // So unwrap is a safe operation on its position
                (&versions).iter().position(|version| {
                    version == version_ref
                }).unwrap()
            });

            // We borrowed versions as the iterator
            // So this requires a second closure
            max_pos.map(move |pos| {
                versions.swap_remove(pos)
            })
        })
    }

    pub fn all_versions(&self) -> io::Result<Box<[BuildVersion]>> {
        fs::read_dir(self.get_directory()).map(|iterator| {
            iterator.filter(|entry| {
                entry.is_ok()
            }).map(|entry| {
                entry.unwrap()
            }).filter(|dir_entry| {
                // Filenames are not always valid rust strings
                // so rust doesn't represent them as such, and we
                // need to convert them 
                dir_entry.file_name().to_str().ok_or(
                    Error::new(ErrorKind::InvalidData, "Could not convert filename to string.")
                ).map(|file_name| {
                    Version::parse(file_name).is_ok()
                }).unwrap_or(false)
            }).map(|dir_entry| {
                let mut file_contents = String::new();
                File::open(dir_entry.path())
                    .and_then(|mut file| {
                        file.read_to_string(&mut file_contents)
                    })
                    .and_then(|_| {
                        BuildVersion::parse_from(&file_contents)
                            .ok_or(Error::new(ErrorKind::InvalidData, "Malformed build version."))
                    })
            }).fold(vec![], |mut vec, version_result| {
                version_result.map(|build_version| {
                    vec.push(build_version)
                });
                vec
            }).into_boxed_slice()
        })
    }

    fn get_directory(&self) -> PathBuf {
       self.parent.get_chain_dir(&self.name)
    }
}

