/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fs;
use std::io;
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};
use semver::Version;

use core::project::chain::Chain;

pub struct Project {
    base_directory : Box<PathBuf>,
    current_chain : Option<String>
}

impl Project {
    fn create_project(path : &Path) -> io::Result<Project> {
        fs::create_dir(path).and_then(|_| {
            fs::create_dir(path.join("chains"))
        }).and_then(|_| {
            fs::create_dir(path.join("logs"))
        }).and_then(|_| {
            fs::File::create(path.join("project"))
        }).and_then(|_| {
            Project::get_project(path)
        })
    }

    
    fn get_project(path : &Path) -> io::Result<Project> {
        Result::Ok(Project {
            base_directory : Box::new(path.to_owned()),
            current_chain : None 
        })
     }

    /**
     * Creates a new chain with a default version number.
     *
     * The default version number is either the one of the current chain,
     * or 0.0.0 if no chain is yet configured.
     */
    fn add_chain_default(&self, chain_name : &String)  -> io::Result<()>{
        self.add_chain(chain_name, {
            // It's legal for a project to have no chains at all
            // We clone because ok_or is transformative
            &(self.current_chain.clone().ok_or(Error::new(ErrorKind::NotFound, "No chain configured.")
            ).and_then(|name| {
               self.get_chain(&name)
            }).and_then(|chain| {
                chain.latest_version().map(|build_version| {
                    build_version.current_version.clone()
                })
            }).unwrap_or(Version {
               major : 0,
               minor : 0,
               patch : 0,
               pre : vec![],
               build : vec![]
            }))
        })
    }

    fn add_chain(&self, chain_name : &String, version : &Version) -> io::Result<()> {
        // If get_chain returns an Ok, we can't make the new chain
        // So err() -> Good if the error is there, bad if not
        self.get_chain(chain_name).err().map_or(
            Err(Error::new(ErrorKind::AlreadyExists, format!("The {} chain already exists.", chain_name))),
            |_| {
            fs::create_dir(self.get_chain_dir(chain_name))
        })
    }

    fn get_chain(&self, chain_name : &String) -> io::Result<Chain> {
        let chain_folder = self.get_chain_dir(chain_name);
        if chain_folder.exists() {
            Ok(Chain::create(self, chain_name))            
        } else {
            Err(Error::new(ErrorKind::NotFound, format!("Could not find given chain: {}",  chain_name)))
        }
    }

    /**
     * Helper for getting directory path of where a chain is stored
     */
    pub fn get_chain_dir(&self, chain_name : &String) -> PathBuf {
        self.base_directory.join("chains").join(chain_name)
    }
}

