/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use semver::{Identifier, Version};
use digest::Digest;
use byteorder::{ByteOrder, BigEndian};

use core::build_products::build_product::BuildProduct;
use core::version::version_change::{VersionChange, increment};

#[derive(Clone, PartialEq)]
pub struct BuildVersion {
    pub current_version : Version,
    hash_result : Box<[u8]>
}

impl BuildVersion {

    fn create_from<D : Digest> (mut digest : D, version : Version) -> BuildVersion {
        BuildVersion {
            current_version : version,
            hash_result : digest.result().as_slice().to_vec().into_boxed_slice()
        }
    }

    pub fn create<D : Digest, B : BuildProduct> (mut digest : D, build_prod : &B, version : &Version) -> BuildVersion {
         // Add build-product bytes to hash
        digest.input(build_prod.to_bytes());
        // Add new version number bytes to hash
        digest.input(&version_to_bytes(&version));

        BuildVersion::create_from(digest, version.clone())
    }

    /**
     * Creates a new BuildVersion by iterating on the previous one.
     */
    pub fn build_on<D : Digest, B : BuildProduct> (&self, mut digest : D, build_prod : &B, version_changes : &[&VersionChange]) -> BuildVersion {
        // Create new version number
        let mut new_version = version_changes.iter().fold(
            self.current_version.clone(),
            |mut version, inc| {
                increment(&mut version, inc);
                version
            }
        );

        // Add build-product bytes to hash
        digest.input(build_prod.to_bytes());
       // Add new version number bytes to hash
        digest.input(&version_to_bytes(&new_version));
        // Add last build's hash to bytes
        digest.input(&self.hash_result);
 
        BuildVersion::create_from(digest, new_version)
    }

    pub fn parse_from(string : &String) -> Option<Self> {
        None
    }
}

fn version_to_bytes(version : &Version) -> Box<[u8]> {
    let mut version_bytes : Vec<u8> = vec![];

    let mut vernum_bytes = vec![0; 4];
    
    // The choice of byte order is purely arbitrary
    for version_num in vec![version.major, version.minor, version.patch] {
        BigEndian::write_u64(&mut vernum_bytes, version_num);
        version_bytes.extend(vernum_bytes.iter().cloned());
    }

    for idents in vec![&version.pre, &version.build] {
        for identifier in idents {
            version_bytes.extend(identifer_to_bytes(identifier).iter().cloned());   
        }
    }

    version_bytes.into_boxed_slice()
}

fn identifer_to_bytes(identifier : &Identifier) -> Box<[u8]> {
    let mut ident_bytes : Vec<u8> = vec![];

    match identifier {
        &Identifier::Numeric(ref number) => {
            let mut number_bytes : Vec<u8> = vec![0; 4];
            BigEndian::write_u64(&mut number_bytes, *number);
            ident_bytes.extend(number_bytes.iter().cloned());
        },
        &Identifier::AlphaNumeric(ref string) => {
            let new_string = string.clone();
            ident_bytes.extend(new_string.into_bytes().iter().cloned());
        }
    }

    ident_bytes.into_boxed_slice()
}

