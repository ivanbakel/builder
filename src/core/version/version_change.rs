/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use semver::{Identifier, Version};
use regex::Regex;
use std::cmp::Ordering;
use std::option::Option;

#[derive(PartialEq)]
pub enum VersionChange {
    Major,
    Minor,
    Patch,
    PreSet(Identifier),
    PreAdd(Identifier),
    PreInc
}

impl PartialOrd for VersionChange {
    fn partial_cmp(&self, other : &Self) -> Option<Ordering> {
        if self == other {
            Some(Ordering::Equal)
        } else {
            match (self, other) {
                (f, s) if f.is_pre() && s.is_pre() => None,
                (f, s) if f.is_pre() => Some(Ordering::Less),
                (f, s) if s.is_pre() => Some(Ordering::Greater),
                (Major, _) => Some(Ordering::Greater),
                (_, Major) => Some(Ordering::Less),
                (Patch, _) => Some(Ordering::Less),
                (_, Patch) => Some(Ordering::Greater),
                // This is not reachable - they aren't equal, and neither is a prerelease or
                // Major/Patch - only Minor is available as an option
                (_, _) => None
            }
        }
    }
}

impl VersionChange {
    pub fn is_pre(&self) -> bool {
        match self {
            &VersionChange::PreSet(_) | &VersionChange::PreAdd(_) | &VersionChange::PreInc => true,
            _ => false
        }
    }

    pub fn parse_change(input : &str) -> Option<Self> {
        lazy_static! {
            static ref major_re : Regex = Regex::new(r"^(?:M|(?i:major))$").unwrap();
            static ref minor_re : Regex = Regex::new(r"^(?:m|(?i:minor))$").unwrap();
            static ref patch_re : Regex = Regex::new(r"^(?i:p|patch)$").unwrap();
        }

        match input {
            i if major_re.is_match(i) =>  {
                Some(VersionChange::Major)
            },
            i if minor_re.is_match(i) => {
                Some(VersionChange::Minor)
            },
            i if minor_re.is_match(i) => {
                Some(VersionChange::Patch)
            }
            _ => {
                None
            }
        }
    }

    pub fn parse_idents(input : &str) -> Option<Box<[Identifier]>> {
        lazy_static! {
            static ref ident_re : Regex = Regex::new(r"^([0-9A-Za-z-]+)((?:\.[0-9A-Za-z-]+)*)$").unwrap();
            static ref numer_re : Regex = Regex::new(r"^[0-9]+$").unwrap();
        }

        let mut current_str = input;
        let mut possible_captures = ident_re.captures(input);
        let mut idents : Option<Vec<Identifier>> = Some(vec![]);

        while idents != None {
            match possible_captures {
                Some(captures) => {
                    // Have to reassign on the map because it counts as a move
                    idents = idents.map(|mut vec| {
                        vec.push({
                            // 0 is the full match - 1 is the first group
                            // The first group is non-optional, so unwrap is safe.
                            let ident = captures.get(1).unwrap().as_str();
                            if numer_re.is_match(ident) {
                                Identifier::Numeric(u64::from_str_radix(ident, 10).unwrap())
                            } else {
                                Identifier::AlphaNumeric(ident.to_string())
                            }
                        });
                        vec
                    });

                    possible_captures = captures.get(2).and_then(|mat| {
                        // Start from 1 to skip the dot separator
                        ident_re.captures(&(mat.as_str()[1..]))
                    });
                },
                None => {
                    idents = None;
                }
            }
        }

        idents.map(|vec| vec.into_boxed_slice())
    }
}

/**
 * Updates the given version by the incremental step
 */
pub fn increment(version : &mut Version, version_change : &VersionChange) {
    match version_change {
        &VersionChange::Major => version.increment_major(),
        &VersionChange::Minor => version.increment_minor(),
        &VersionChange::Patch => version.increment_patch(),
        &VersionChange::PreSet(ref ident) => version.pre = vec![ident.clone()],
        &VersionChange::PreAdd(ref ident) => version.pre.push(ident.clone()),
        &VersionChange::PreInc => increment_prerelease(version)
    }
}

pub fn finalize(version : &mut Version) {
    version.pre = vec![];
}

/**
 * Increments the final prerelease identifier.
 *
 * If the last identifier is numeric, adds 1 to it. Otherwise, adds an additional Numeric identifier
 * starting at 1.
 */
pub fn increment_prerelease(version : &mut Version) {
    match version.pre.last() {
        Some(&Identifier::Numeric(num)) => {
            version.pre.pop();
            version.pre.push(Identifier::Numeric(num + 1))
        },
        _ => {
            version.pre.push(Identifier::Numeric(1));
        }    
    }
}

