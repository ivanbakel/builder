/* builder - build product versioning tool 
Copyright (C) 2017  Isaac van Bakel 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

extern crate semver;
extern crate byteorder;
extern crate digest;
extern crate digest_buffer;
extern crate generic_array;
extern crate regex;
#[macro_use]
extern crate lazy_static;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

mod core;

/*
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}*/
